package cz.sdc.application;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages = "cz.sdc")
public class PrimeNumbersCheckerApplication {


    public static void main(String[] args) {
        try {
            SpringApplication.run(PrimeNumbersCheckerApplication.class, args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}