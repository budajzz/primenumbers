package cz.sdc.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExcelParser implements IExcelParser{

    public final List<BigInteger> getPrimeNumbersFromExcel(@NonNull final String pathToFile) throws Exception {

        final Workbook workbook = WorkbookFactory.create(new FileInputStream(pathToFile));
        final Sheet sheet = workbook.getSheetAt(0);
        final List<BigInteger> primeNumbers = new ArrayList<>();

        for (final Row row : sheet) {
            final Cell cell = row.getCell(1);
            if (cell != null && cell.getCellType() == CellType.STRING) {
                if (StringUtils.isNumeric(cell.getStringCellValue())) {
                    final BigInteger numberToCheck = new BigInteger(cell.getStringCellValue());
                    if (numberToCheck.signum() == 1 && numberToCheck.isProbablePrime(1)) {
                        primeNumbers.add(numberToCheck);
                    }
                }
            }
        }
        return primeNumbers;
    }
}
