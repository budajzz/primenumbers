package cz.sdc.service;

import org.springframework.lang.NonNull;

import java.math.BigInteger;
import java.util.List;

public interface IExcelParser {
    List<BigInteger> getPrimeNumbersFromExcel(@NonNull final String pathToFile) throws Exception;

}
