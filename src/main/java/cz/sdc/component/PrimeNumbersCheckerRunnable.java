package cz.sdc.component;

import cz.sdc.service.ExcelParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.List;
import java.util.Scanner;

@Component
public class PrimeNumbersCheckerRunnable implements CommandLineRunner {

    private ExcelParser excelParser;

    @Autowired
    public PrimeNumbersCheckerRunnable(@NonNull final ExcelParser excelParser) {
        this.excelParser = excelParser;
    }


    @Override
    public void run(final String... args) throws Exception {
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            System.out.println("For stop to program type exit or ");
            System.out.print("enter Path to file: ");
            final String pathToFile = scanner.next();

            if (pathToFile.equals("exit")) {
                System.out.println("Exiting");
                System.exit(0);
            } else if (!(pathToFile.indexOf(".xlsx") > -1)) {
                System.out.println("invalid excel file");
                continue;
            }

            final List<BigInteger> primeNumbersFromExcel = excelParser.getPrimeNumbersFromExcel(pathToFile);
            if (primeNumbersFromExcel.isEmpty()) {
                System.out.println("prime numbers not found");
            } else {
                for (final BigInteger primeNumber : primeNumbersFromExcel) {
                    System.out.println(primeNumber);
                }
            }
        }
    }
}
